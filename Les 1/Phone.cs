﻿namespace Les_1
{
    public class Phone
    {
        private string _merk;
        private string _type;
        private string _omschrijving;
        private int _prijs;

        public Phone(string merk, string type, string omschrijving, int prijs)
        {
            _merk = merk;
            _type = type;
            _omschrijving = omschrijving;
            _prijs = prijs;
        }

        public string Merk => _merk;

        public string Type => _type;

        public string Omschrijving => _omschrijving;

        public int Prijs
        {
            get => _prijs;
            set => _prijs = value;
        }
        
    }
}