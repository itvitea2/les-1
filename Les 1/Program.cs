﻿using System;
using System.Collections.Generic;

namespace Les_1
{
    class Program
    {
        // ReSharper disable once ArrangeTypeMemberModifiers
        static void Main(string[] args)
        {
            MainMenu();
        }

        // ReSharper disable once FunctionRecursiveOnAllPaths
        private static void MainMenu()
        {
            Console.CursorVisible = false;
            Console.Title = "Phone Tool";
            var phones = new List<Phone>
            {
                new("Huawei", "P30", "6.47\" FHD+ (2340x1080) OLED, Kirin 980 Octa-Core (2x Cortex-A76 2.6GHz + 2x Cortex-A76 1.92GHz + 4x Cortex-A55 1.8GHz), 8GB RAM, 128GB ROM, 40+20+8+TOF/32MP, Dual SIM, 4200mAh, Android 9.0 + EMUI 9.1", 697),
                new("Samsung", "Galaxy A52", "64 megapixel camera, 4k videokwaliteit 6.5 inch AMOLED scherm 128 GB opslaggeheugen (Uitbreidbaar met Micro-sd) Water-en stofbestendig (IP67)", 399),
                new("Apple", "IPhone 11", "Met de dubbele camera schiet je in elke situatie een perfecte foto of video De krachtige A13-chipset zorgt voor razendsnelle prestaties Met Face ID hoef je enkel en alleen naar je toestel te kijken om te ontgrendelen Het toestel heeft een lange accuduur dankzij een energiezuinige processor", 619),
                new("Google", "Pixel 4a", "12.2 megapixel camera, 4k videokwaliteit 5.81 inch OLED scherm 128 GB opslaggeheugen 3140 mAh accucapaciteit", 411),
                new("Xiaomi", "Redmi Note 10 Pro", "108 megapixel camera, 4k videokwaliteit 6.67 inch AMOLED scherm 128 GB opslaggeheugen (Uitbreidbaar met Micro-sd) Water-en stofbestendig (IP53)", 298)
            };
            while (true)
            {

                Console.WriteLine("Phone Tool - Main menu");
                Console.WriteLine();
                Console.WriteLine("0. Afsluiten");
                for (var i = 0; i < phones.Count; i++)
                {
                    Console.WriteLine(i + 1 + ". " + phones[i].Merk + " " + phones[i].Type);
                }

                Console.WriteLine("Kies een optie: ");
                var input = Console.ReadKey().KeyChar;

                Console.Clear();
                if (char.IsDigit(input))
                {
                    var selection = (int)Char.GetNumericValue(input);

                    if (selection <= phones.Count && selection != 0)
                    {
                        selection--;
                        var selectedPhone = phones[selection];
                        Console.WriteLine(selectedPhone.Merk + " - " + selectedPhone.Type + " - $" + selectedPhone.Prijs);
                        Console.WriteLine();
                        Console.WriteLine(selectedPhone.Omschrijving);
                        Console.ReadKey();
                        Console.Clear();
                        MainMenu();
                    }
                    else if (selection == 0)
                    {
                        Environment.Exit(0);
                    }
                }

                Console.Clear();
            }
            // ReSharper disable once FunctionNeverReturns
        }
    }
}